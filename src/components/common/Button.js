import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

const Button = ({ onPress, btnName, txtStyle }) => {

    const { btnStyle, textStyle } = styles;

    return (
        <TouchableOpacity onPress={onPress} style={btnStyle}>
            <Text style={txtStyle || textStyle}>{btnName}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    btnStyle: {
      alignSelf: 'center',
      backgroundColor: '#fff',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#2089dc',
      backgroundColor: '#2089dc',
    },
    textStyle: {
      color: '#ffff',
      textAlign: 'center',
      paddingTop: 10,
      paddingBottom: 10,
      paddingRight: 20,
      paddingLeft: 20,
      fontSize: 16
    }
};

export default Button;