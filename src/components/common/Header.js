import React, { Component } from 'react';
import { Header } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';


const CustomHeader = ({ loggedIn }) => {
    
    return (
        <Header 
            leftComponent={{ icon: 'home', color: '#fff',  underlayColor: '#2089dc', onPress: () => Actions.movies() }}
            centerComponent={{ text: 'MOVIES', style: { color: '#fff' }}}
            rightComponent={ loggedIn && { icon: 'exit-to-app', color: '#fff',  underlayColor: '#2089dc', onPress: () => firebase.auth().signOut() }}
        />
    );
};

export default CustomHeader;