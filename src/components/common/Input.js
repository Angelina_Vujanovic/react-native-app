import React from 'react';
import { TextInput, View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry, containerStyle, labelStyle, inputStyle, icon, onIconClick, multiline }) => {

  const { input, lblStyle, wrapperStyle, inlineStyle, iconStyle } = styles;

  const renderInput = () => {
    if(icon) {
      return (
        <View style={inlineStyle}>
          <TextInput
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            autoCorrect={false}
            style={inputStyle || input}
            value={value}
            onChangeText={onChangeText}
            multiline={multiline}
          />
            <Icon
              name={icon}
              color={'#2089dc'}
              size={20}
              containerStyle={iconStyle}
              onPress={onIconClick}
            />
        </View>
      );
    }
    return (
        <TextInput
          secureTextEntry={secureTextEntry}
          placeholder={placeholder}
          autoCorrect={false}
          style={inputStyle || input}
          value={value}
          onChangeText={onChangeText}
          multiline={multiline}
        />
    )
  }

  return (
    <View style={containerStyle || wrapperStyle}>
      <Text style={labelStyle || lblStyle}>{label}</Text>
          { renderInput() }
    </View>
  );
};

const styles = {
  input: {
    color: '#000',
    fontSize: 18,
    flex: 1
  },
  lblStyle: {
    fontSize: 10,
  },
  wrapperStyle: {
    height: 50,
    marginBottom: 30,
    marginLeft: 30,
    marginRight: 30,
    borderBottomWidth: 1,
    borderColor: '#3DC6FA',
  },
  inlineStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1
  },
  iconStyle: {
    padding: 10
  }
};

export default Input;