import React from 'react';
import { Image, View, Text } from 'react-native';
import Spinner from './Spinner';

const LoadScreen = () => {

    return (
        <View style={styles.containerStyle}>            
            <Image
                style={styles.imageStyle}
                source = {require('../../../assets/films.jpg')} 
            />
            <Spinner/>
            <Text style={styles.textStyle}>Wait a moment ...</Text>
        </View>
    );
};

const styles = {
    imageStyle: {
        width: 100,
        height: 100,
        marginBottom: 20
    },
    textStyle: {
        color: '#2089dc',
        fontWeight: 'bold',
        marginTop: 20
    },
    containerStyle: {
        alignItems:'center',
        justifyContent: 'center',
        flexDirection: 'column',
        flex: 1
    }
};

export default LoadScreen;