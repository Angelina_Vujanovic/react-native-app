import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Button from '../common/Button';

class Movie extends Component {

    render() {        
        const { movie } = this.props;

        return (
            <View style={styles.containerStyle}>

                <View style={styles.inlineStyle}>
                    <Text style={styles.titleTextStyle}>{movie.title}</Text>
                    <Text style={{paddingTop: 3}}> ({movie.year})</Text>
                </View>

                <View style={styles.inlineStyle}>
                    <Text>{movie.runtime} min | </Text>
                    <Text>{movie.genre.join(", ")}</Text>
                </View>

                <View style={styles.descriptionStyle}>
                    <Text style={{textAlign:'justify'}}>{movie.description}</Text>
                </View>

                <View style={[styles.inlineStyle, {justifyContent:'space-between'}]}>

                    <View style={styles.inlineStyle}>
                        <Icon
                            name='star'
                            color='#a67c00'
                            size={20}
                            containerStyle={styles.ratingIconStyle}
                        /> 
                        <Text>{movie.rating}/10</Text>   
                    </View>

                    <View style={{marginTop: 10}}>
                        <Text>{movie.votes} votes</Text>
                    </View>

                    <View style={styles.inlineStyle}>
                        <View style={styles.metascoreStyle}>
                            <Text>{movie.metascore}</Text>
                        </View>
                        <Text> metascore</Text>
                    </View>

                </View>

                <View style={{marginTop:10}}>
                    <Text style={styles.sectionTitleStyle}>Cast and Crew</Text>
                    <Text style={{fontWeight: 'bold', marginTop: 10}}>Top Billed Cast:</Text>
                    <Text style={{paddingLeft: 5 }}>{movie.actors.join("\r\n")}</Text>
                    <View style={styles.inlineStyle}>
                        <Text style={{fontWeight: 'bold'}}>Director: </Text>
                        <Text>{movie.director}</Text>
                    </View>
                </View>
                
                <View style={{marginTop:10, marginBottom: 20}}>
                    <Text style={styles.sectionTitleStyle}>Box office</Text>
                    <View style={styles.inlineStyle}>
                        <Text style={{fontWeight: 'bold'}}>Opening weekend: </Text>
                        <Text>${movie.revenue || 0}</Text>
                    </View>
                </View>

                <Button 
                    btnName="Add comment" 
                    onPress={()=> Actions.comments({id: movie.rank})}
                />
            </View>
        );
    }
   
};

const styles = {
    containerStyle: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop:20
    },
    titleTextStyle: {
        fontWeight: 'bold',
        fontSize: 20,
        maxWidth: 250
    },
    ratingIconStyle: {
        width: 20,
        height: 20
    },
    descriptionStyle: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#D0D1DA',
        marginTop: 10,
        padding: 10
    },
    inlineStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 10
    },
    columnStyle: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionTitleStyle: {
        fontSize: 18,
        color: '#a67c00'
    },
    metascoreStyle: {
        padding: 3,
        backgroundColor: '#66cc33',
        color: '#ffff'
    }
};

export default Movie;