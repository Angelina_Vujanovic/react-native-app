import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';


class MovieCard extends PureComponent {

    render() {
        const { containerStyle, iconStyle, rowStyle, ratingStyle, textStyle, ratingIconStyle} = styles;
        const { movie } = this.props;

        return (
            <View style={containerStyle}>
                <TouchableOpacity onPress={() => Actions.selectedMovie({ movie })} style={rowStyle}>
                    <View>
                        <Text style={textStyle}>{movie.title} ({movie.year})</Text>
                        <View style={ratingStyle}>
                            <Icon
                                name='star'
                                color='#a67c00'
                                size={15}
                                containerStyle={ratingIconStyle}
                            /> 
                            <Text>{movie.rating}</Text>
                        </View>
                    </View>
                    <Icon
                        name='keyboard-arrow-right'
                        color='#2089dc'
                        size={20}
                        containerStyle={iconStyle}
                    />
                </TouchableOpacity>
            </View>
        )
    }
   
};

const styles = {
    containerStyle: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: '#D0D1DA'
    },
    rowStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconStyle: {
        width: 15,
        height: 15,
        marginRight: 5,
        marginBottom: 5
    },
    textStyle: {
        fontWeight: 'bold',
        maxWidth: 220
    },
    ratingIconStyle: {
        width: 15,
        height: 15
    },
    ratingStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export default MovieCard;