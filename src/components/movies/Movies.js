import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import MovieCard from './MovieCard';
import { movies } from '../../constants/movies';

class Movies extends Component {

    render() {
        const { containerStyle } = styles; 

        return (
            <View style={containerStyle}>
                <FlatList
                    data={movies}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item}) => <MovieCard movie={item} />}
                    keyExtractor={item => item.rank}
                />
            </View>
        )
    }

}

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 50
    }
}

export default Movies;
