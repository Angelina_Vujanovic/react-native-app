import React, { Component } from 'react';
import { TouchableOpacity, View, Keyboard} from 'react-native';
import { Icon } from 'react-native-elements';
import firebase from 'firebase';
import moment from 'moment';
import Input from '../common/Input';
import Spinner from '../common/Spinner';

class AddComment extends Component {

    state = { comment: '', error: '', loading: false };
    
    send = () => {
        const { comment } = this.state;
        const { id } = this.props;
        const { currentUser } = firebase.auth();

        this.setState({loading: true});
        
        Keyboard.dismiss();

        const currentDate = moment().format('llll');

        firebase.database().ref(`/movies/${id}/comments`)
        .push({ text: comment, user: currentUser.email, date: currentDate })
        .then(()=> this.setState({comment:'', loading: false}));
    }

    render() {
        const { commentStyle, wrapperStyle, inputStyle, iconStyle } = styles;
        
        return (
            <View style={commentStyle} >
                 <Input
                    placeholder="Enter your comment"
                    containerStyle={wrapperStyle}
                    inputStyle={inputStyle}
                    value={this.state.comment}
                    multiline
                    onChangeText={(comment) => this.setState({comment})}
                />
                {
                    this.state.loading ? 
                        <Spinner />
                    :
                        <TouchableOpacity onPress={this.send} disabled={!this.state.comment}>
                            <Icon
                                name='send'
                                color={!this.state.comment ? '#f1f1f1' : '#2089dc'}
                                size={20}
                                containerStyle={iconStyle}
                            />
                        </TouchableOpacity>
                }  
            </View>
        )
    }
}

const styles = {
    commentStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItem: 'center',
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10
    },
    inputStyle: {
        color: '#000',
        fontSize: 18,
        maxWidth: 200
    },
    wrapperStyle: {
        height: 50,
        marginBottom: 20,
        flex: 1
    },
    iconStyle: {
        marginTop: 10
    }
};

export default AddComment;