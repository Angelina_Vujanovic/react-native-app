import React, {Component} from 'react';
import { View, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';
import firebase from 'firebase';
import { commentsFetch } from '../../state/comments/actions';
import Spinner from '../common/Spinner';
import AddComment from '../comments/AddComment';

class Comments extends Component {

    componentWillMount() {
        this.props.commentsFetch(this.props.id);  
    }

    componentWillUnmount() {
        firebase.database().ref(`/movies/${this.props.id}/comments`).off('value');
    }

    renderItem(comment) {
        
        const { commentStyle, authorStyle } = styles;

        return (
            <View style={commentStyle} key={comment.text}>
                <Text style={authorStyle}>{comment.user} · {moment(comment.date).fromNow()}</Text> 
                <Text>{comment.text}</Text>   
            </View> 
        );
    }

    renderComments(movieComments) {
        if(this.props.inProgress) {
            return (<Spinner />);
        }

        if(movieComments.length === 0) {
            return (
                <Text style={styles.noCommentStyle}>No comments yet</Text>
            );
        }

        return ( 
            <FlatList 
                data={movieComments}
                renderItem={({item}) => this.renderItem(item)}
                keyExtractor={item => item.uid}
            />
        );
    }
    

    render() {
        const { commentsSectionStyle } = styles;
   
        const movieComments = this.props.content ? _.map(this.props.content, (val, uid) => {return { ...val, uid }; }) : [];

        return (
            <View style={commentsSectionStyle}>
                <AddComment id={this.props.id} />
                { this.renderComments(movieComments.reverse()) }
            </View>
        );
       
    }
}

const styles = {
    commentsSectionStyle: {
        marginRight: 10,
        marginLeft: 10,
        marginTop: 5,
        flex: 1
    },
    commentStyle: {
        borderTopWidth: 1,
        borderColor: '#D0D1DA',
        maxHeight: 250,
        flex: 1,
        paddingVertical: 5
    },
    noCommentStyle: {
        textAlign: 'center',
        fontSize: 18
    },
    authorStyle: {
        fontWeight: 'bold',
        marginBottom: 5
    }
};

const mapStateToProps = ( {comments: { content, inProgress }}) => {
    return { content, inProgress };
};
  
export default connect(mapStateToProps, { commentsFetch })(Comments);
