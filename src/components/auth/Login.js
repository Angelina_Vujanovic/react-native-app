import React, { Component } from 'react';
import { Text, KeyboardAvoidingView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import Input from "../common/Input";
import Button from "../common/Button";
import Spinner from '../common/Spinner';

class Login extends Component {
    
    state = { email: '', password: '', error: '', loading: false, show: false }

    login = () => {

        const { email, password } = this.state;

        this.setState({ error: '', loading: true });

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(()=> Actions.main())
        .catch(() => {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(()=> Actions.main())
            .catch((error)=> this.setState({ error: error.message, loading: false }));
        });
    };

    render() {
        const { containerStyle, errorStyle } = styles;

        return (
            <KeyboardAvoidingView style={containerStyle} behavior="padding" >
                <Input
                    placeholder="Enter your email"
                    label="Email"
                    value={this.state.email}
                    onChangeText={email =>  this.setState({ email, error:'' })}
                />
                <Input
                    secureTextEntry={!this.state.show}
                    placeholder="Enter your password"
                    label="Password"
                    value={this.state.password}
                    icon={this.state.show ? 'visibility' : 'visibility-off' }
                    onIconClick={()=> this.setState({show: !this.state.show})}
                    onChangeText={password => this.setState({ password, error:'' })}
                />
                <Text style={errorStyle}>{this.state.error}</Text>
                {
                    this.state.loading ? 
                        <Spinner />
                    :
                        <Button 
                            btnName="Login" 
                            onPress={this.login}
                        />
                }
            </KeyboardAvoidingView>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    errorStyle: {
        color: '#FA3877',
        textAlign: 'center',
        marginBottom: 10
    },
    imageStyle: {
        width: 100,
        height: 100,
        alignSelf: 'center',
        marginBottom: 20
    }
};

export default Login;