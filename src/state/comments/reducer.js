import {
    COMMENTS_FETCH, COMMENTS_FETCH_SUCCESS, COMMENTS_FETCH_FAIL
  } from './types';
  
  const INITIAL_STATE = {
    content: {},
    inProgress: false,
    error: ""
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case COMMENTS_FETCH:
        return { ...state, inProgress: true };
      case COMMENTS_FETCH_SUCCESS:
        return { ...state, inProgress: false, content: action.payload };
      case COMMENTS_FETCH_FAIL:
      return { ...state, inProgress: false, error: "Error has ocurred" };
      default:
        return state;
    }
  };
  