import firebase from 'firebase';
import { 
  COMMENTS_FETCH, 
  COMMENTS_FETCH_SUCCESS, 
  COMMENTS_FETCH_FAIL
}
from './types';



export const commentsFetch = (movieId) => {

  return (dispatch) => {
    dispatch({type: COMMENTS_FETCH});
    firebase.database().ref(`/movies/${movieId}/comments`)
      .on('value', snapshot => {
        dispatch({ type: COMMENTS_FETCH_SUCCESS, payload: snapshot.val() });
      });
  };
};



