import { combineReducers } from 'redux';
import CommentsReducer from './comments/reducer';

export default combineReducers({
    comments: CommentsReducer
});
