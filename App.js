import React from 'react';
import { Provider } from 'react-redux';
import firebase from 'firebase';
import store from './src/store/store';
import Router from './Router';
import { connection } from './src/constants/config';

export default class App extends React.Component {


  componentWillMount() {

    firebase.initializeApp(connection);
    
  }
 

  render() {

    return (
      <Provider store={store}>
         <Router />  
      </Provider>
    );
  }
}

