# React-native-app

#### Running the APP
To download necessary libraries run the following command:
```
npm install
```
After that, run the following command:
```
expo start
```

```
Download the Expo application for the mobile.
Scan the QR code above with the Expo app (Android) or the Camera app (iOS).
```
