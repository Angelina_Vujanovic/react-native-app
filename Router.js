import React, { Component } from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import Login from './src/components/auth/Login';
import Movies from './src/components/movies/Movies';
import Movie from './src/components/movies/Movie';
import Comments from './src/components/comments/Comments';
import CustomHeader from './src/components/common/Header';
import LoadScreen from './src/components/common/LoadScreen';

class RouterComponent extends Component {
    
    state = { loggedIn : null};

    componentWillMount() {
        
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });
    }

    render() {
        return (
            <Router sceneStyle={styles.container}>
                <Scene key="root" hideNavBar>
                    <Scene 
                        key="loading"
                        component={LoadScreen} 
                        initial={this.state.loggedIn===null}
                    />
                    <Scene 
                        key="login" 
                        component={Login} 
                        title="" 
                        initial={this.state.loggedIn===false} 
                    />
                    <Scene 
                        key="main" 
                        initial={this.state.loggedIn===true} 
                        navBar={CustomHeader} 
                        loggedIn={this.state.loggedIn}
                    >
                        <Scene
                            key="movies"
                            component={Movies}
                            title=""
                            initial
                        />
                        <Scene 
                            key="selectedMovie" 
                            component={Movie} 
                            title="" 
                        />
                        <Scene 
                            key="comments" 
                            component={Comments} 
                            title="" 
                        />
                    </Scene>
                </Scene>
            </Router>
        );
    }
        
};


const styles = {
    container: {
      flex: 1,
      backgroundColor: '#fff'
    }
};

export default RouterComponent;
